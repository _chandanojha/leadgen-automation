import re
import requests
from bs4 import BeautifulSoup
from service import find_exact_date
__author__ = 'chandanojha'

def extract_titles(one_class):
	result = one_class.find_all('h2', {"class": 'jobTitle'})
	title_text = result[0].text
	if title_text.startswith('new'):
		title_text = 'new ' + title_text[3:]
		return title_text
	else:
		return title_text


def extract_company_name(one_class):
	result = one_class.find_all('span', {"class": 'companyName'})
	return result[0].text


def extract_job_location(one_class):
	result = one_class.find_all('div', {"class": 'companyLocation'})
	return result[0].text


def extract_posting_date_of_job(one_class):
	result = one_class.find_all('span', {"class": 'date'})
	result = result[0].text
	if len(result.split()) > 1 and result.split()[0].isdigit():
		exact_date = find_exact_date(result.split()[0])
		return exact_date.date().isoformat()
	elif result == 'Today' or result == 'Just posted':
		exact_date = find_exact_date(0)
		return exact_date.date().isoformat()
	else:
		return result


def extract_description(one_class):
	result = one_class.find_all('div', {"class": 'job-snippet'})
	result = result[0].text.split('\n')
	return list(filter(None, result))


def extract_original_company_url(job_url):
	page = requests.get(job_url)
	soup2 = BeautifulSoup(page.text, 'html.parser')
	result_url = soup2.find_all('div', {'id': 'applyButtonLinkContainer'})
	final_result = [page]
	if result_url:
		res = result_url[0].find('a').get('href')
		final_result.append(res)
	else:
		final_result.append('Apply From Indeed')
	return final_result


def extract_company_rating(one_class):
	result = one_class.find_all('span', {"class": 'ratingsDisplay'})
	return float(result[0].text) if result else 'Not Found'


def extract_id(url):
	url_res = url.split('?jk=')
	print(url_res[1].split('&')[0])
	return url_res[1].split('&')[0]
	

def extract_unique_job_id(page, job_url, c_website):
	if '?jk=' in job_url:
		return extract_id(job_url)

	elif '?jk=' in c_website:
		return extract_id(c_website)

	else:
		p = re.compile(r"jk='(.*?)'")
		id = p.findall(page.text)
		print(id[0])
		return id[0]

