import datetime
import pandas as pd
from db_config_connection import fetch_config_data

__author__ = 'chandanojha'


def config_parameters_for_script():
	script_config = {}
	data = fetch_config_data()
	script_config['urls'] = data[0]
	script_config['total_no_of_pages'] = data[1]
	script_config['job_titles'] = data[2]
	script_config['locations'] = data[3]
	return script_config


def find_exact_date(delta):
	tod = datetime.datetime.now()
	d = datetime.timedelta(days=int(delta))
	a = tod - d
	return a


def get_no_of_pages(val):
	return [i * 10 for i in range(val[0])]


def check_url_formatting(url):
	if not url.endswith('jobs?q='):
		return url + 'jobs?q='
	return url


def export_data_to_file(filename, new_data):
	try:
		existing_data = pd.read_excel(filename)
		new_dataframe = pd.concat([existing_data, new_data]).drop_duplicates(subset='Unique ID').reset_index(drop=True)
		new_dataframe.to_excel(filename, index=False)
	except FileNotFoundError:
		new_data.to_excel(filename, index=False)


def extract_domain(url):
	import urllib.parse as parser
	url = parser.urlparse(url)
	subdomain = url.hostname.split('.')[0]
	if subdomain == 'www':
		return 'usa'
	return subdomain
