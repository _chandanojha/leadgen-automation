import pymysql as MySQLdb
from db_config import config_for_database

__author__ = 'chandanojha'


def check_or_create_table(dbcur):
	table = '''
	CREATE TABLE `timestamp` (
  	`id` int NOT NULL AUTO_INCREMENT,
  	`start_time` datetime(6) ,
  	`end_time` datetime(6) ,
  	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
	'''
	tablename = 'timestamp'
	dbcur.execute("""
	        SELECT COUNT(*)
	        FROM information_schema.tables
	        WHERE table_name = '{0}'
	        """.format(tablename))
	if dbcur.fetchone()[0] == 1:
		return dbcur
	else:
		dbcur.execute(table)
	return dbcur


def update_runtime(start_time):
	dbcur = timestamp_connection()
	dbcur = check_or_create_table(dbcur)
	
	# update start time
	dbcur.execute()


def update_endtime(end_time):
	dbcur = timestamp_connection()
	
	# update endtime
	dbcur.execute()


def timestamp_connection():
	host = config_for_database("host")
	user = config_for_database('user')
	password = config_for_database('password')
	database = config_for_database('database')
	db = MySQLdb.connect(host=host, user=user, password=password, database=database)
	dbcur = db.cursor()
	return dbcur

# disconnect from server
# db.close()
