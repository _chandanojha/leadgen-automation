import pymysql as MySQLdb
from db_config import config_for_database
__author__ = 'chandanojha'


def db_connection():
	
	host = config_for_database("host")
	user = config_for_database('user')
	password = config_for_database('password')
	database = config_for_database('database') or None
	db = MySQLdb.connect(host=host, user=user, password=password, database=database)
	cursor = db.cursor()
	
	cursor.execute("show tables")
	
	tables = cursor.fetchall()
	
	try:
		if not tables[0][0] == 'config':
			print('No table with the name "configuration" is present')
	except IndexError:
		print('No configuration table found in the database.')
	
	fetch_config_data = 'SELECT * FROM config;'
	cursor.execute(fetch_config_data)
	
	config_data = cursor.fetchall()
	db.close()
	return config_data


def fetch_config_data():
	config_data = db_connection()
	urls = []
	page = []
	job_titles = []
	locations = []
	for row in config_data:
		try:
			for url in row[1].split(','):
				urls.append(url)
			
			page.append((row[2]))
				
			for title in row[3].split(','):
				job_titles.append(title)
				
			for location in row[4].split(','):
				locations.append(location)
		except IndexError:
			continue
	
	return [urls, page, job_titles, locations]
