import datetime
import pandas as pd
from db_timestamp_connection import update_runtime, update_endtime
from extract_components import *
from service import get_no_of_pages, export_data_to_file, extract_domain, check_url_formatting, \
	config_parameters_for_script

__author__ = 'chandanojha'

'''
some other parameters
&fromage=15&sort=date
fromage = no of days in past to get results
&jt=all = &jt=fulltime = job type
&sr=directhire for excluding staffing agencies

'''
# Base_Url = 'https://www.indeed.com/jobs?q='
filename = ''


def Driver_function():
	start_time = datetime.datetime.now()
	# update_runtime(start_time)
	
	configs = config_parameters_for_script()
	urls = configs['urls']
	total_no_of_pages = configs['total_no_of_pages'][0]
	job_titles = configs['job_titles']
	locations = configs['locations']
	final_no_of_pages = get_no_of_pages([total_no_of_pages])
	extra_url_params = '&sort=date&jt=fulltime&sr=directhire'

	for url in urls:
		url = check_url_formatting(url)
		Base_Url = url
		for one_location in locations:
			one_location = one_location.strip().replace(" ", "+")
			for one_job_title in job_titles:
				global filename
				filename = one_job_title.replace(" ", "_").capitalize() + '.xlsx'
				one_job_title = one_job_title.strip().replace(" ", "+")
				for one_page in final_no_of_pages:
					URL = Base_Url + one_job_title + '&l=' + one_location + '&start=' + str(one_page) + extra_url_params
					print('\n', URL, '\n')
					page = requests.get(URL)
					soup = BeautifulSoup(page.text, 'html.parser')
					all_job_classes = find_all_job_classes(soup)
					domain = extract_domain(url)
					data = extract_all_data(all_job_classes, domain)
					export_data_to_file(filename, data)
					print(one_location, one_job_title, one_page)

	end_time = datetime.datetime.now()
	# update_endtime(end_time)
	print(end_time - start_time)
	print('Scrapping Complete')


def find_all_job_classes(soup):
	jobs = soup.find_all('a', {'class': 'tapItem'})
	return jobs


def extract_all_data(all_job_classes, domain):
	description = []
	for one_class in all_job_classes:
		job = {}
		# extracting job title
		job['domain'] = domain

		job['Job Title'] = extract_titles(one_class)
		
		# extracting company name
		job['Company Name'] = extract_company_name(one_class)
		
		# extracting job location
		job['Location'] = extract_job_location(one_class)
		
		# extract date
		job['Posting Date'] = extract_posting_date_of_job(one_class)
		
		job['Rating'] = extract_company_rating(one_class)
		
		# extract job description
		desc = extract_description(one_class)
		job['Description'] = "\n ".join(map(str, desc))
		
		href = one_class['href']
		job_url = 'https://www.indeed.com' + href
		job['Job Url From Indeed'] = job_url
		# extracting  company url
		result = extract_original_company_url(job_url)
		page, c_website = result[0], result[1]
		# the result will return 2 components one is page and the other is company url
		job['Company Website'] = result[1]
		print(job_url, c_website)
		unique_id = extract_unique_job_id(page, job_url, c_website)
		job['Unique ID'] = unique_id
		job['is_processed'] = 0
		description.append(job)
	df = pd.DataFrame(description)
	return df


Driver_function()
